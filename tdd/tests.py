from django.test import TestCase, Client
from tdd.views import landing_page, create_status
from django.urls import resolve
from django.http import HttpRequest
from tdd.models import Question

# Create your tests here.

class LandingPageTest(TestCase):
    def testUrl(self):
        c = Client()
        response = c.get('/tdd/')
        self.assertTrue(response.status_code, 200)

    def testContent(self):
        self.assertIsNotNone(landing_page)
        self.assertIsNotNone(create_status)

    def testContain(self):
    	request = HttpRequest()
    	response = landing_page(request)
    	html_response = response.content.decode('utf8')
    	self.assertIn('<title>TDD</title>', html_response)
    	self.assertIn('<h1>“Hello, Apa kabar?”</h1>', html_response)

    def testCanPost(self):
    	new_status = Question.objects.create(question_text='hello')
    	allstatus = Question.objects.all().count()
    	self.assertEqual(allstatus,1)

    
