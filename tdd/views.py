from django.shortcuts import render, redirect
from .models import Question
from .forms import FormStatus
# Create your views here.

def landing_page(request):
    questions = Question.objects.all()
    response = { "Question" : questions }
    return render(request, 'tdd/landing-page.html', response)

def create_status(request):
	response = {}
	questions = Question.objects.all()
	response = { "Question" : questions , "form" : FormStatus}

	form =  FormStatus(request.POST or None)
	if (request.method == "POST"):
		if(form.is_valid()):
			question_text = request.POST.get("question_text")
			Question.objects.create(question_text=question_text)
			return render(request, 'tdd/landing-page.html',response)
		else:
			return render(request, 'tdd/landing-page.html',response)
	else:
		response['form'] = form
		return render(request, 'tdd/landing-page.html', response)

